import java.util.ArrayList;

/**
 * Knight
 * @author Josh
 */
public class Knight extends ChessPiece {

    public Knight(Player p, Square s) 
    {
        super(p, s);
    }
    
    int[][] movematrix = { 
        {1, 2}, {1, -2}, {-1, 2}, {-1, -2}, 
        {2, 1}, {2, -1}, {-2, 1}, {-2, -1} 
    };

    @Override
    public ArrayList<Square> evaluateValidMoves(Board board) {
        ArrayList<Square> validmoves = new ArrayList<>();
        
        for (int[] move : movematrix) {
            Square square = this.getMove(move, 1);
            if (board.isValidSquare(square)) {
                if (board.isCollision(square)) {
                    if (board.get(square).player != player) {
                        validmoves.add(square);
                    }
                } else {
                    validmoves.add(square);
                }
            }
        }
        
        return validmoves;
    }
    
    @Override
    public String getSymbol() {
        return this.player == Player.White ? "WN" : "BN";
    }
    
}
