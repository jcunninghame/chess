import java.util.ArrayList;
import java.util.HashMap;

/**
 * Chessboard Representation
 * @author Josh
 */
public class Board extends HashMap<Square, ChessPiece> {
    
    private final int ranks = 8; // rows
    private final int files = 8; // columns
    
    /**
     * Chessboard represented by 64 squares mapped to chess pieces
     */
    public Board() { }
    
    /**
     * Get all the pieces on the board
     * @return Array of ChessPieces
     */
    public ArrayList<ChessPiece> getAllPieces()
    {
        return new ArrayList<>(this.values());
    }
    
    /**
     * Place a new ChessPiece on the board
     * @param newpiece ChessPiece to be placed
     */
    public void placeChessPiece(ChessPiece newpiece)
    {
        if (isCollision(newpiece.getLocation())) {
            System.out.println("Cannot place " + newpiece.toString() + ".  Occupied by " + this.get(newpiece.getLocation()).toString() + ".");
        }
        else {
            this.put(newpiece.getLocation(), newpiece);
            updateAllValidMoves();
        }
    }
    
    /**
     * Remove a ChessPiece from the board
     * @param square Location of the chess piece to be removed
     * @return ChessPiece removed if successful, null if failed
     */
    public ChessPiece removeChessPiece(Square square)
    {
        ChessPiece piece;
        if (!isCollision(square)) {
            System.out.println("Cannot remove from " + square.toString() + ".  No piece exists.");
            piece = null;
        } else {
            piece = this.remove(square);
            updateAllValidMoves();
        }
        return piece;
    }
    
    /**
     * Moves a ChessPiece from one square to another.  
     * Start square must contain a ChessPiece
     * Destination square must be a valid move
     * Takes opponents piece if possible
     * @param oldsquare Square of ChessPiece to move
     * @param newsquare Move destination
     */
    public void moveChessPiece(Square oldsquare, Square newsquare)
    {
        ChessPiece piece = this.get(oldsquare);
        if (piece == null) {
            System.out.println("Cannot move from " + oldsquare.toString() + ".  No piece exists.");
        } else if (piece.validmoves == null || !piece.validmoves.contains(newsquare)) {
            System.out.println("Cannot move to " + newsquare.toString() + ".  Invalid move!");
            System.out.print(piece.toString() + " has moves: [");
            piece.getValidMoves().stream().forEach((square) -> {
                System.out.print(" " + square.toString());
            });
            System.out.println(" ]");
        }
        
        piece = removeChessPiece(oldsquare);        
        if (isCollision(newsquare)) {
            ChessPiece takenpiece = removeChessPiece(newsquare);
            System.out.println(piece.toString() + " takes " + takenpiece.toString());
        }        
        piece.location = newsquare;
        piece.validmoves = null;
        placeChessPiece(piece);
    }    

    /**
     * Update the valid moves for each ChessPiece on the Board
     */
    public void updateAllValidMoves() {
        getAllPieces().stream().forEach((chesspiece) -> {
            chesspiece.updateValidMoves(this);
        });
    }
    
    /**
     * Tests if the square contains a ChessPiece
     * @param square Square to test
     * @return True is square occupied, otherwise false
     */
    public boolean isCollision(Square square)
    {
        return this.get(square) != null;
    }
    
    /**
     * Tests if the square is on the board
     * @param square Square to test
     * @return True if square is on the board, otherwise false
     */
    public boolean isValidSquare(Square square)
    {
        return 0 < square.getFile() && square.getFile() <= ranks && 
               0 < square.getRank() && square.getRank() <= files;
    }
    
    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = ranks; i > 0; i--){
            sb.append("  ---------------------------------\r\n");
            sb.append(i);
            for (int j = 1; j < files + 1; j++){                
                sb.append(" |");
                Square square = new Square(j, i);
                sb.append(this.get(square) != null ? this.get(square).getSymbol() : "  ");
            }
            sb.append(" |\r\n");
        }
        sb.append("  ---------------------------------\r\n");
        sb.append("    a   b   c   d   e   f   g   h");
        return sb.toString();
    }
    
    /**
     * Prints the board including possible moves for a given ChessPiece
     * @param chesspiece ChessPiece to show possible moves
     * @return String representation of board
     */
    public String toString(ChessPiece chesspiece) {
        ArrayList<Square> validmoves = chesspiece.getValidMoves();
        StringBuilder sb = new StringBuilder();
        for (int i = ranks; i > 0; i--){
            sb.append("  ---------------------------------\r\n");
            sb.append(i);
            for (int j = 1; j < files + 1; j++){                
                sb.append(" |");
                Square square = new Square(j, i);
                if (validmoves.contains(square)){
                    sb.append(chesspiece.getSymbol().toLowerCase());
                } else
                {
                    sb.append(this.get(square) != null ? this.get(square).getSymbol() : "  ");
                }                
            }
            sb.append(" |\r\n");
        }
        sb.append("  ---------------------------------\r\n");
        sb.append("    a   b   c   d   e   f   g   h");
        return sb.toString();
    }
}
