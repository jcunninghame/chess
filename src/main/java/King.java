import java.util.ArrayList;

/**
 * King
 * @author Josh
 */
public class King extends ChessPiece {

    public King(Player p, Square s) {
        super(p, s);
    }
    
    int[][] movematrix = { 
        {1, 1},  {0, 1},  {-1, 1},
        {1, 0},           {-1, 0},
        {1, -1}, {0, -1}, {-1, -1}
    };

    @Override
    public ArrayList<Square> evaluateValidMoves(Board board) {
        ArrayList<Square> validmoves = new ArrayList<>();
        
        for(int[] move : movematrix) {
            Square square = this.getMove(move, 1);
            if (board.isValidSquare(square)) {
                if (board.isCollision(square)){
                    if (board.get(square).player != player) {
                        validmoves.add(square);
                    }
                } else {
                    validmoves.add(square);
                }
            }
        }
        
        return validmoves;
    }
    
    @Override
    public String getSymbol() {
        return this.player == Player.White ? "WK" : "BK";
    }
    
}
