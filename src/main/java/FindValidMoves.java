import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

/**
 * Main
 * Application Entry
 * @author Josh
 */
public class FindValidMoves {
    
    static final Pattern usagepattern = Pattern.compile("usage");
    static final Pattern addpattern = Pattern.compile("[BW][\\s][KQBNRP][\\s][a-h][1-8]");
    static final Pattern movepattern = Pattern.compile("[a-h][1-8][\\s][a-h][1-8]");
    static final Pattern allmovespattern = Pattern.compile("all");
    static final Pattern movespattern = Pattern.compile("[a-h][1-8]");
    static final Pattern resetpattern = Pattern.compile("reset");
    static final Pattern boardpattern = Pattern.compile("board");
    static final Pattern exitpattern = Pattern.compile("exit");

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        
        System.out.println("Welcome to FindValidMoves.  Type 'usage' for instructions.");
        Board board = new Board();        
        boolean cont = true;
        
        // Main loop
        while(cont)
        {
            System.out.print("FVM > ");
            
            // Get input from user
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String inputstr;
            try {
                inputstr = br.readLine();
            } catch (IOException ioe) {
               System.out.println("IO error trying to read input!");
               break;
            }
            
            // PARSE INPUT OPTIONS:
            
            // Add ChessPiece
            if (addpattern.matcher(inputstr).matches()) {
                ChessPiece newpiece = ParseChessPiece(inputstr);
                System.out.println("Placing " + newpiece.toString() + "...");
                board.placeChessPiece(newpiece);
                
            } // Moves chess piece from one square to another
            else if (movepattern.matcher(inputstr).matches()) {
                Square[] squares = ParseMove(inputstr);
                System.out.println("Moving " + board.get(squares[0]).toString() + " to " + squares[1].toString() + "...");
                board.moveChessPiece(squares[0], squares[1]);
            
            } // Print all the valid moves on the board 
            else if (allmovespattern.matcher(inputstr).matches()) {
                PrintAllMoves(board);      
                
            } // Print the valid moves for a square 
            else if (movespattern.matcher(inputstr).matches()) {
                Square inputsquare = new Square(inputstr.charAt(0), Integer.parseInt(inputstr.substring(1)));
                ChessPiece piece = board.get(inputsquare);
                if (piece != null){
                    PrintValidMovesForPiece(board, piece);
                } else {
                    System.out.println("Square [" + inputsquare.toString() + "] has no chesspiece.");
                }             
                
            } // Stop the application
            else if (exitpattern.matcher(inputstr).matches()) {
                System.out.println("Stopping!");
                break;   
                
            } // Reset the board 
            else if (resetpattern.matcher(inputstr).matches()) {
                System.out.println("Resetting board!");
                board.clear(); 
                
            } // Print the board to console 
            else if (boardpattern.matcher(inputstr).matches()) {
                System.out.println(board.toString());  
                
            } // Print usage instructions
            else if (usagepattern.matcher(inputstr).matches()) {
                PrintUsage();
                
            } // Otherwise prompt for instructions
            else {
                System.out.println("Unrecognised input!  Enter 'usage' for details");
            }
        }        
    }
    
    public static void PrintValidMovesForPiece(Board board, ChessPiece piece)
    {                       
        System.out.println(board.toString(piece));
        System.out.print(piece.toString() + " has moves: [");
        piece.getValidMoves().stream().forEach((square) -> {
            System.out.print(" " + square.toString());
        });
        System.out.println(" ]");
    }
    
    public static void PrintAllMoves(Board board)
    {
        System.out.println("Valid moves for current board...");
        board.getAllPieces().stream().forEach((piece) -> {
            System.out.print(piece.toString() + " has moves: [");
            piece.getValidMoves().stream().forEach((square) -> {
                System.out.print(" " + square.toString());
            });
            System.out.println(" ]");
        });
    }
    
    public static Square[] ParseMove(String input)
    {
        String[] inputs = input.split("\\s");
        return new Square[]
        {
            new Square(inputs[0].charAt(0), Integer.parseInt(inputs[0].substring(1))),
            new Square(inputs[1].charAt(0), Integer.parseInt(inputs[1].substring(1)))
        };
    }
    
    public static ChessPiece ParseChessPiece(String input) {        
        String[] inputs = input.split("\\s");
        
        Player player = inputs[0].equals("W") ? Player.White : Player.Black;        
        Square square = new Square(inputs[2].charAt(0), Integer.parseInt(inputs[2].substring(1)));
        
        switch (inputs[1]){
            case "K":
                return new King(player, square);
            case "Q":
                return new Queen(player, square);
            case "B":
                return new Bishop(player, square);
            case "N":
                return new Knight(player, square);
            case "R":
                return new Rook(player, square);
            case "P":
                return new Pawn(player, square);
            default:
        }
        
        return null;
    }
    
    public static void PrintUsage()
    {
        System.out.println("Usage:");
        System.out.println("[B|W] [K|Q|B|N|R|P] [a-h][1-8] : Adds a new chesspiece to the board");
        System.out.println("\t E.g: \"B B d1\" adds Black Bishop @ (d,1)");
        System.out.println("[a-h][1-8] : Shows valid moves if a chesspiece is on square");
        System.out.println("\t E.g: \"e2\" shows valid moves for pieces on (e,2)");
        System.out.println("[[a-h][1-8] [a-h][1-8]: Moves a piece from one square to another square");
        System.out.println("\t E.g: \"d1 d4\" moves piece from d1 -> d4");
        System.out.println("all   : Shows valid moves for all pieces");
        System.out.println("board : Shows the current board");
        System.out.println("usage : Shows usage instructions");
        System.out.println("reset : Resets the board to blank");
        System.out.println("exit  : Exits the application");
    }
}
