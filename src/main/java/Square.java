import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Square representation
 * Each square is represented by its file/rank coordinates
 * @author Josh
 */
public class Square {
    
    private final int file; // column
    private final int rank; // row    
    
    public Square(int f, int r)
    {        
        file = f;
        rank = r;
    }
    
    public Square(char f, int r)
    {        
        file = f - 96;
        rank = r;
    }

    /**
     * Gets the file coordinate
     * @return file co-ord
     */
    public int getFile() {
        return file;
    }

    /**
     * Gets the rank coordinate
     * @return rank co-ord
     */
    public int getRank() {
        return rank;
    }
    
    @Override
    public String toString()
    {
        return (char)(file + 96) + "," + rank;
    }
    
    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Square))
            return false;
        if (obj == this)
            return true;
        
        Square objsquare = (Square) obj;
        return new EqualsBuilder().
            append(file, objsquare.file).
            append(rank, objsquare.rank).
            isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 19).
            append(file).
            append(rank).
            toHashCode();
    }
}
