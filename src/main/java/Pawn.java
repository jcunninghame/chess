import java.util.ArrayList;

/**
 * Pawn
 * @author Josh
 */
public class Pawn extends ChessPiece {

    
    public Pawn(Player p, Square s) {
        super(p, s);
    }
    
    private boolean firstMove()
    {
        return player == Player.White 
                ? this.location.getRank() == 2
                : this.location.getRank() == 7;
    }

    @Override
    public ArrayList<Square> evaluateValidMoves(Board board) {
        ArrayList<Square> validmoves = new ArrayList<>();
        
        // Check if pawn can move forward
        Square fwd = this.getMove(new int[] {0,1}, 1);
        if (board.isValidSquare(fwd) && !board.isCollision(fwd)) {
            validmoves.add(fwd);
        }
        
        // Check if pawn can take
        int[][] movematrix = { {0,1}, {0,-1} };
        for (int[] move : movematrix ){
            Square square = this.getMove(move, 1);
            if (board.isValidSquare(square) && board.isCollision(square)) {
                if (board.get(square).player != player) {
                    validmoves.add(square);
                }
            }
        }
        
        // Check if pawn can double move on first play
        if (firstMove()) {
            Square square = this.getMove(new int[] {0, 2}, 1);
            if (board.isValidSquare(square) && !board.isCollision(square)) {
                validmoves.add(square);
            }
        }
        
        return validmoves;
    }
    
    @Override
    public String getSymbol() {
        return this.player == Player.White ? "WP" : "BP";
    }
}
