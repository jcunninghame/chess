import java.util.ArrayList;

/**
 * Bishop
 * @author Josh
 */
public class Bishop extends ChessPiece {
    
    public Bishop(Player p, Square s) 
    {
        super(p, s);
    }
    
    int[][] movematrix = { 
        {1, 1}, {-1, 1}, {1, -1}, {-1, -1}
    };

    @Override
    public ArrayList<Square> evaluateValidMoves(Board board) {
        ArrayList<Square> validmoves = new ArrayList<>();
        
        for(int[] move : movematrix) {
            int i = 1;
            for (Square square = this.getMove(move, i); // Run this move
                board.isValidSquare(square);            // Until an invalid square is reached
                i++, square = this.getMove(move, i)) {  // increment square along move direction
                if (board.isCollision(square)) {
                    if (board.get(square).player != player) {
                        validmoves.add(square);
                    }
                    break; // Break on collision with another piece
                }
                validmoves.add(square);
            }
        }
        
        return validmoves;
    }    

    @Override
    public String getSymbol() {
        return this.player == Player.White ? "WB" : "BB";
    }
}