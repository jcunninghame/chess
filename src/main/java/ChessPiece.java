import java.util.ArrayList;

/**
 * ChessPiece super class
 * All ChessPieces derive from this class
 * @author Josh
 */
public abstract class ChessPiece {
    
    protected final Player player;
    protected Square location;
    protected ArrayList<Square> validmoves;
    
    public ChessPiece(Player p, Square s)
    {
        player = p;
        location = s;
    }

    /**
     * Gets the square occupied by the ChessPiece
     * @return Locus Square
     */
    public Square getLocation() {
        return location;
    }

    /**
     * Gets the player colour of the ChessPiece
     * @return Player colour
     */
    public Player getPlayer() {
        return player;
    }
    
    /**
     * Returns all the valid moves this ChessPiece can make
     * @return Array of Squares representing valid moves
     */
    public ArrayList<Square> getValidMoves()
    {
        return validmoves;
    }
    
    /**
     * Updates the valid moves for this ChessPiece
     * @param board Current board state
     */
    public void updateValidMoves(Board board){
        validmoves = this.evaluateValidMoves(board);
    }
    
    /**
     * Gets a square by projecting a vector
     * @param direction Direction vector (file,rank)
     * @param distance Number of squares to project by
     * @return Square location of projection
     */
    protected Square getMove(int[] direction, int distance)
    {
        return player == Player.White
               ? new Square(location.getFile() + (direction[0] * distance), location.getRank() + (direction[1] * distance))
               : new Square(location.getFile() - (direction[0] * distance), location.getRank() - (direction[1] * distance));
    }
    
    /**
     * Evaluates the valid moves for this ChessPiece
     * Each derived piece needs to implement its own function
     * @param board Current board state
     * @return Array of valid squares
     */
    protected abstract ArrayList<Square> evaluateValidMoves(Board board);
    
    /**
     * Gets the symbol for this ChessPiece [Colour,Type]
     * @return ChessPiece symbol
     */
    public abstract String getSymbol();
    
    @Override
    public String toString()
    {
        return player.name() + " " + this.getClass().getSimpleName() + " @ " + location.toString();
    }
    
}